apt update -y && apt upgrade -y
apt install -y sshguard nmap bmon screenfetch zsh curl wget vim
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
echo 'screenfetch' >> .zshrc
